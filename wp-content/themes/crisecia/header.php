<?php

/**

 * The header for our theme

 *

 * This is the template that displays all of the <head> section and everything up until <div id="content">

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package crisecia

 */

global $configuracao;

?>

<!doctype html>

<html <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="https://gmpg.org/xfn/11">



	<!-- FAVICON -->

	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" /> 



	<?php wp_head(); ?>

</head>



<body <?php body_class(); ?> >
	<!-- TOPO -->
	<header class="topo">
		<div class="containerFull">
			<div class="row">
				<!-- LOGO -->
				<div class="col-sm-3">
					<a href="<?php echo get_home_url(); ?>">
						<img class="" src="<?php echo $configuracao['opt_logo']['url'] ?>"  alt="Logo Chris & Cia">
					</a>
				</div>
				<!-- MENU  -->	
				<div class="col-sm-9">
					<div class="navbar" role="navigation">	
						<!-- MENU MOBILE TRIGGER -->
						<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
							<span class="sr-only"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!--  MENU MOBILE-->
						<div class="row navbar-header">			
							<nav class="collapse navbar-collapse" id="collapse">
								<?php 
								$menu = array(
									'theme_location'  => '',
									'menu'            => 'Menu Principal',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'nav navbar-nav',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 2,
									'walker'          => ''
									);
								wp_nav_menu( $menu );
							?>
								<div class="abrirPesquisa">
									<a href=""><i class="fas fa-search"></i></a> 
								</div>	
							</nav>						
						</div>			
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- PÁGINA DE NEWSLETTER -->
	<div class="lenteNewsletter"  style="display:none;" >
		<div class="pg pg-newsletter">
			<!-- IMAGEM NEWS -->
			<figure>
				<span class="fecharModalNewsletter" style="display: none;"><i class="fas fa-times"></i></span>
				<img src="<?php echo $configuracao['opt_imagemNewsLetter']['url'] ?>" alt="Cavalo correndo">
			</figure>
			<!-- ÁREA DE CADASTRO -->
			<article>
				<div class="textosArticle">
					<p><i class="far fa-envelope"></i> <strong>Cadastre seu e-mail</strong> e receba as melhores ofertas!</p>
					<input type="text" name="nome" placeholder="Nome">
					<input class="email" type="text" name="email" placeholder="E-mail">
					<input type="submit" name="cadastrar" value="CADASTRAR">
				</div>
			</article>
		</div>
	</div>

