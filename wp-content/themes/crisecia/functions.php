<?php

/**

 * crisecia functions and definitions

 *

 * @link https://developer.wordpress.org/themes/basics/theme-functions/

 *

 * @package crisecia

 */



if ( ! function_exists( 'crisecia_setup' ) ) :

	/**

	 * Sets up theme defaults and registers support for various WordPress features.

	 *

	 * Note that this function is hooked into the after_setup_theme hook, which

	 * runs before the init hook. The init hook is too late for some features, such

	 * as indicating support for post thumbnails.

	 */

	function crisecia_setup() {

		/*

		 * Make theme available for translation.

		 * Translations can be filed in the /languages/ directory.

		 * If you're building a theme based on crisecia, use a find and replace

		 * to change 'crisecia' to the name of your theme in all the template files.

		 */

		load_theme_textdomain( 'crisecia', get_template_directory() . '/languages' );



		// Add default posts and comments RSS feed links to head.

		add_theme_support( 'automatic-feed-links' );



		/*

		 * Let WordPress manage the document title.

		 * By adding theme support, we declare that this theme does not use a

		 * hard-coded <title> tag in the document head, and expect WordPress to

		 * provide it for us.

		 */

		add_theme_support( 'title-tag' );



		/*

		 * Enable support for Post Thumbnails on posts and pages.

		 *

		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/

		 */

		add_theme_support( 'post-thumbnails' );



		// This theme uses wp_nav_menu() in one location.

		register_nav_menus( array(

			'menu-1' => esc_html__( 'Primary', 'crisecia' ),

		) );



		/*

		 * Switch default core markup for search form, comment form, and comments

		 * to output valid HTML5.

		 */

		add_theme_support( 'html5', array(

			'search-form',

			'comment-form',

			'comment-list',

			'gallery',

			'caption',

		) );



		// Set up the WordPress core custom background feature.

		add_theme_support( 'custom-background', apply_filters( 'crisecia_custom_background_args', array(

			'default-color' => 'ffffff',

			'default-image' => '',

		) ) );



		// Add theme support for selective refresh for widgets.

		add_theme_support( 'customize-selective-refresh-widgets' );



		/**

		 * Add support for core custom logo.

		 *

		 * @link https://codex.wordpress.org/Theme_Logo

		 */

		add_theme_support( 'custom-logo', array(

			'height'      => 250,

			'width'       => 250,

			'flex-width'  => true,

			'flex-height' => true,

		) );

	}

endif;

add_action( 'after_setup_theme', 'crisecia_setup' );



/**

 * Set the content width in pixels, based on the theme's design and stylesheet.

 *

 * Priority 0 to make it available to lower priority callbacks.

 *

 * @global int $content_width

 */

function crisecia_content_width() {

	// This variable is intended to be overruled from themes.

	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.

	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound

	$GLOBALS['content_width'] = apply_filters( 'crisecia_content_width', 640 );

}

add_action( 'after_setup_theme', 'crisecia_content_width', 0 );



/**

 * Register widget area.

 *

 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar

 */

function crisecia_widgets_init() {

	register_sidebar( array(

		'name'          => esc_html__( 'Sidebar', 'crisecia' ),

		'id'            => 'sidebar-1',

		'description'   => esc_html__( 'Add widgets here.', 'crisecia' ),

		'before_widget' => '<section id="%1$s" class="widget %2$s">',

		'after_widget'  => '</section>',

		'before_title'  => '<h2 class="widget-title">',

		'after_title'   => '</h2>',

	) );

}

add_action( 'widgets_init', 'crisecia_widgets_init' );



/**

 * Enqueue scripts and styles.

 */

function crisecia_scripts() {

	//FONTS

	wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i');

	wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.1.0/css/all.css');



	//CSS

	wp_enqueue_style( 'crisecia-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');

	wp_enqueue_style( 'crisecia-owlcarousel', get_template_directory_uri() . '/css/owl.carousel.css');

	wp_enqueue_style( 'crisecia-style', get_stylesheet_uri() );



	

	//JAVA SCRIPT

	wp_enqueue_script( 'crisecia-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js' );

	wp_enqueue_script( 'crisecia-bootsrap', get_template_directory_uri() . '/js/bootstrap.min.js' );

	wp_enqueue_script( 'crisecia-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js' );

	wp_enqueue_script( 'crisecia-geral', get_template_directory_uri() . '/js/geral.js' );





	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {

		wp_enqueue_script( 'comment-reply' );

	}

}

add_action( 'wp_enqueue_scripts', 'crisecia_scripts' );



/**

 * Implement the Custom Header feature.

 */

require get_template_directory() . '/inc/custom-header.php';



/**

 * Custom template tags for this theme.

 */

require get_template_directory() . '/inc/template-tags.php';



/**

 * Functions which enhance the theme by hooking into WordPress.

 */

require get_template_directory() . '/inc/template-functions.php';



/**

 * Customizer additions.

 */

require get_template_directory() . '/inc/customizer.php';



/**

 * Load Jetpack compatibility file.

 */

if ( defined( 'JETPACK__VERSION' ) ) {

	require get_template_directory() . '/inc/jetpack.php';

}





	/************************************

	FUNÇÕES EXTRAS 

	************************************/

		//REDUX FRAMEWORK

		if (class_exists('ReduxFramework')) {

			require_once (get_template_directory() . '/redux/sample-config.php');

		}



		//FUNÇÃO PARA REGISTRAR MENU

		function register_my_menus() {

			register_nav_menus(

				array(

					'crisecia' => __('MenuPrincipal'),

				)

			);

		}

		add_action( 'init', 'register_my_menus' );



		//FUNÇÃO DE ABREVIAÇÃO DE CARACTER

		function customExcerpt($qtdCaracteres) {

		  $excerpt = get_the_excerpt();

		  $qtdCaracteres++;

		  if ( mb_strlen( $excerpt ) > $qtdCaracteres ) {

		    $subex = mb_substr( $excerpt, 0, $qtdCaracteres - 5 );

		    $exwords = explode( ' ', $subex );

		    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );

		    if ( $excut < 0 ) {

		      echo mb_substr( $subex, 0, $excut );

		    } else {

		      echo $subex;

		    }



		    echo '...';

		  }else{

		    echo $excerpt;

		  }

		}



		//PAGINAÇÃO

		function pagination($pages = '', $range = 4){

		    $showitems = ($range * 2)+1;

		    global $paged;

		    if(empty($paged)) $paged = 1;



		    if($pages == ''){

		        global $wp_query;

		        $pages = $wp_query->max_num_pages;

		        if(!$pages){

		            $pages = 1;

		        }

		    }



		    if(1 != $pages){



		        echo "<div class=\"paginador\"><ul>";

		        //if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";

		        //if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";

				$htmlPaginas = "";

		        for ($i=1; $i <= $pages; $i++){

		            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){

		                $htmlPaginas .= ($paged == $i)? '<li><a href="' . get_pagenum_link($i) . '" class="ativo">' . $i . '</a></li>' : '<li><a href="' . get_pagenum_link($i) . '" class="numero">' . $i . '</a></li>';

		            }



		        }

		        if ($paged < $pages && $showitems < $pages) echo '<li><a href="' . get_pagenum_link($paged + 1) . '" class="esquerda"><img src="'. get_template_directory_uri() .'/img/flechaEsquerdaPaginador.png" alt="" class="setaEsquerdaPaginador"></a></li>';

		        	echo $htmlPaginas;



				if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo '<li><a href="' . get_pagenum_link($pages) . '"><img src="'. get_template_directory_uri() .'/img/flechaDireitaPaginador.png" alt="" class="setaDireitaPaginador"></a></li>';



		        	echo "</ul></div>\n";



			}



		}