<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package crisecia
 */
global $configuracao;
?>
	<!-- RODAPÉ -->
	<footer class="rodape">
		<div class="containerFull">
			<div class="row">
				<div class="col-sm-6">
					<div class="logoRodape">
						<a href="<?php echo get_home_url(); ?>">
							<figure>
								<img src="<?php echo $configuracao['opt_logoRodape']['url'] ?>" alt="Logo Cris e Cia">
							</figure>
						</a>	
					</div>
				</div>

				<div class="col-sm-6">
					<div class="redesSociais">
						<span>Siga-nos nas redes sociais</span>
						<!-- REDES SOCIAIS -->
						<div class="divRedesSociais">
							<?php if($configuracao['opt_facebook']): ?>
							<a href="<?php echo $configuracao['opt_facebook']; ?>" class="facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
						<?php endif;

							if($configuracao['opt_instagram']): 
						 ?>
							<a href="<?php echo $configuracao['opt_instagram']; ?>" class="instagram" target="_blank"><i class="fab fa-instagram"></i></a>
						<?php endif;

							if($configuracao['opt_youtube']) :
						 ?>
							<a href="<?php echo $configuracao['opt_youtube']; ?>" class="youtube" target="_blank"><i class="fab fa-youtube"></i></a>
						<?php endif; ?>
						</div>
					</div>
				</div>
			</div>

			<hr>

			<div class="row">
				<div class="col-sm-3">
					<div class="descricaoCrisECia">
						<p><?php echo $configuracao['opt_descricaoRodape']; ?></p>
					</div>
				</div>
				
				<div class="col-sm-3">
					<div class="menuRodape">
						<span>Menu</span>
						
						<?php 
								$menu = array(
									'theme_location'  => '',
									'menu'            => 'Menu Principal',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => '',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 2,
									'walker'          => ''
									);
								wp_nav_menu( $menu );
							?>		
					</div>
				</div>
				
				<div class="col-sm-2">
					<div class="categoriasRodape">
						<span>Categorias</span>
						<ul>
							<?php  
								$categoriasRodape = get_categories();
								
								foreach ($categoriasRodape as $categoriasRodape):
									if($categoriasRodape->name != 'destaque' && $categoriasRodape->name != 'Sem categoria' && $categoriasRodape->name != 'Destaque'):
							?>
								<li><a href="<?php echo get_permalink(); ?>"><?php echo $categoriasRodape->name ;?></a></li>
							<?php endif; endforeach; ?>
						</ul>
					</div>
				</div>
				
				<div class="col-sm-4">
					<div class="paginaFacebook">
						<div class="areaInfo">
		                    <div id="fb-root"></div>
			                   <script>
				                   	(function(d, s, id) {
				                     var js, fjs = d.getElementsByTagName(s)[0];
				                     if (d.getElementById(id)) return;
				                     js = d.createElement(s); js.id = id;
				                     js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.3&appId=1636093796635204";
				                     fjs.parentNode.insertBefore(js, fjs);
				                   }(document, 'script', 'facebook-jssdk'));
			               		</script>
		                   
				                  <!-- <script>(function(d, s, id) {
				                     var js, fjs = d.getElementsByTagName(s)[0];
				                     if (d.getElementById(id)) return;
				                     js = d.createElement(s); js.id = id;
				                     js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.4";
				                     fjs.parentNode.insertBefore(js, fjs);
				                   }(document, 'script', 'facebook-jssdk'));</script>-->
		                   
                          	 	<div class="fb-page" data-href="https://www.facebook.com/modacountry" data-width="400" data-height="false" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/modacountry"><a href="https://www.facebook.com/modacountry">Cris e cia Moda Country</a></blockquote></div></div>
		           
		                   		<!--<div class="fb-page" data-href="https://www.facebook.com/modacountry" data-width="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/modacountry"><a href="https://www.facebook.com/modacountry">Facebook</a></blockquote></div></div>
		                  			-->
	              		 	</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<!-- COPYRIGHT -->
	<div class="copyright">
		<div class="containerFull">
			<div class="row">
				<div class="col-sm-4">
					<div class="cnpj">
						<p>CRIS & CIA MODA COUNTRY | CNPJ 05885379000188 | IE 415.123.826.110
						   RUA EPHRAIN RODRIGUES ALVES, 416 JARDIM CLAUDIA - LEME/ SP - CEP
						</p>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="textoCopyright">
						<p>
							© Todos os direitos reservados. Eventuais promoções, descontos e prazos de pagamento expostos aqui são válidos apenas para compras via internet. 
							As fotos, textos e layout aqui veiculados são de propriedade da Loja. É proibida a utilização total ou parcial sem nossa autorização.
						</p>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="logoHandragnAreaCopyright">
						<a href="http://handgran.com/" target="_blank">
							<figure>
								<img src="img/logoHandgran.png" alt="Logo Handgran">
							</figure>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php wp_footer(); ?>

</body>
</html>
