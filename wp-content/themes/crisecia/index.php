<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package crisecia
 */

get_header();
?>

	<!-- PG INICIAL -->
	<div class="pg pg-inicial" style="display:;">  
		<!-- CONTAINER BOOTSTRAP -->
		<div class="containerFull">
			<!-- ROW DESTAQUES -->
			<div class="row">
				<!-- COLUNA BOOTSTRAP -->
				<div class="col-md-7">
					<!-- CARROSSEL DE DESTAQUE -->
					<section class="carrosselDestaque sessao">
						<h6 class="hidden">Sessão de posts destaques</h6>
						<?php 
							//LOOP DE POST CATEGORIA DESTAQUE				
							$postsCategory = new WP_Query(array(
								'post_type'     => 'post',
								'posts_per_page'   => 5,
								'tax_query'     => array(
									array(
										'taxonomy' => 'category',
										'field'    => 'slug',
										'terms'    => 'destaque',
										)
									)
								)
							);
						?>
						<div id="carrosselDestaque" class="owl-Carousel">
							<?php 
								while ($postsCategory->have_posts()) : 
									$postsCategory->the_post(); 

									//FOTO DESTACADA
									$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$fotoPost = $fotoPost[0];

									global $post;
									$categories = get_the_category();

							?>
								<!-- ITEM CARROSSEL -->
								<a href="<?php echo get_permalink() ?>">
									<figure class="item" style="background: url(<?php echo $fotoPost ?>);">
										<div class="textoPostPrincipal">
											<img src="<?php echo $fotoPost ?>" alt="Imagem destaque">
											<!-- CATEGORIA DO POST -->
											<?php 
												foreach ($categories as $categories){
													if ($categories->name != "destaque"){
														$nomeCategoria = $categories->name;
													}
												} 
											?>									
											<h3><?php echo $nomeCategoria; ?></h3>
											<!-- TITULO DO POST -->
											<h2><?php echo get_the_title(); ?></h2>
											<!-- DATA DO POST -->
											<strong><?php echo  get_the_date('j F, Y'); ?></strong>
										</div>
									</figure>
								</a>
							<?php  endwhile; wp_reset_query();  ?>
						</div>
					</section>
				</div>

				<!-- COLUNA BOOTSTRAP -->
				<div class="col-md-5">
					<!-- LISTA DE POSTS MAIS LIDOS -->
					<div class="postsMaisLidos">
						<ul>	
							<!-- LOOP DE POSTS -->
							<?php 
								$postsRandomicos = new WP_Query( 
									array( 
										'post_type' => 'post',
										'orderby' => 'rand', 
										'posts_per_page' => 3 
									) 
								);
								

								if ( $postsRandomicos->have_posts() ) : while( $postsRandomicos->have_posts() ) : 
									$postsRandomicos->the_post();
									
									$imagemDestacada = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									
									$imagemDestacada = $imagemDestacada[0];
									
									global $post;
									
									$categories = get_the_category();
							?>
							<!-- ITEM POST MAIS LIDO -->
							<li>
								<a href="<?php echo get_permalink() ?>">
									<!-- IMAGEM DE FUNDO -->
									<figure style="background:url(<?php echo $imagemDestacada ?>" alt="">
									</figure>
									<!-- INFORMAÇÕES DO POST -->
									<article class="informacoesPost">
										<!-- CATEGORIA DO POST -->
										<?php  
										foreach ($categories as $categories){
											if ($categories->name != "destaque"){
												$nomeCategoria = $categories->name;
											}
										} ?>
										<h3><?php echo $nomeCategoria; ?></h3>

										<!-- TÍTULO DO POST -->
										<h2><?php echo get_the_title(); ?></h2>
										<!-- DATA DO POST -->
										<span><?php echo  get_the_date('j F, Y'); ?></span>
									</article>
								</a>
							</li>
							<?php   endwhile; endif; wp_reset_query(); ?>

						</ul>
					</div>
				</div>
			</div>
			
			<!-- DIV PRODUTOS EM DESTAQUE -->
			<div class="carrosselProdutos" style="">
				<!-- SESSÃO DO CARROSSEL -->
				<section class="carrosselProdutosDestaque">
					<div id="carrosselProdutosDestaque" class="owl-Carousel">
						<?php 	
							$produtos = new WP_Query( array( 'post_type' => 'produtos', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
							while ( $produtos->have_posts() ) : $produtos->the_post();
				
								// FOTO DESTACADA
								$fotoDestaqueProduto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$fotoDestaqueProduto = $fotoDestaqueProduto[0];

								// METABOX
								$precoDoProduto = rwmb_meta('Crisecia_precoProduto');

								$precoTransformado= str_replace(".",",",$precoDoProduto);

								$parcelamentoProduto = rwmb_meta('Crisecia_parcelamentoProduto');

								$descontoProduto = rwmb_meta('Crisecia_descontoProduto');

								$verificacaoDesconto = rwmb_meta('Crisecia_verificacaoCheckboxDesconto');

								$linkDoProduto = rwmb_meta('Crisecia_linkProduto');

								$english_format_number = number_format($number);

								if($verificacaoDesconto != 0){ 
									$precoAtual = ($precoTransformado - (($precoTransformado/100)*$descontoProduto));
									//TRANSFORMAÇÃO DE PONTO PARA VIRGULA
									$precoAtualTransformado = str_replace(".",",",$precoAtual);
								}
								
						?>
							<!-- ITEM -->
							<div class="item itemProdutoDestaque">
								<a href="<?php echo $linkDoProduto; ?>" target="_blank">
									<?php if($verificacaoDesconto != 0): ?>
										<span class="porcentagemDesconto">- <?php echo $descontoProduto .'%'; ?></span>
									<?php endif; ?>
									<figure>
										<img src="<?php echo $fotoDestaqueProduto; ?>" alt="Imagem">
									</figure>

									<h3><?php 	echo get_the_title();; ?></h3>

									<hr>

									<span class="classificacaoProduto">
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
									</span>

									<?php if($verificacaoDesconto != 0): ?>
										<small class="precoSemDesconto">
											R$ <?php echo $precoTransformado; ?>
										</small>
									<?php else:  ?>

										<small class="precoSemDesconto" style="visibility: hidden;">
											R$ <?php echo $precoTransformado; ?>
										</small>

									<?php endif; ?>

									<h4 class="precoAtual">
										<strong>
											R$ <?php echo $precoAtualTransformado; ?>
										</strong> à vista</h4>

									<?php if($parcelamentoProduto > 0):
										$precoParcelado = $precoAtualTransformado/$parcelamentoProduto;

										$precoDaParcela = number_format($precoParcelado, 2, ',', ' ');
									?>
										<small class="precoParcelado">
											ou <?php echo $parcelamentoProduto . 'x'?> de R$ <?php 
											echo ($precoDaParcela); ?> sem juros 
										</small>
									<?php endif; ?>
									
								</a>
							</div>
						<?php   endwhile; wp_reset_query(); ?>
					

					</div>
					<div class="botoesCarrosselProdutos">
						<button id="flechaEsquerda"></button>
						<button id="flechaDireita"></button>
					</div>
				</section>
			</div>

			<!-- ROW SESSÃO DE POSTS -->
			<div class="row">
				<div class="col-md-9">
					<!-- SESSÃO DE POSTS -->
					<section class="sessaoPosts">
						<?php 
					        
							if ( have_posts() ) : while( have_posts() ) : the_post();
								$fotoDestaquePost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								
								$fotoDestaquePost = $fotoDestaquePost[0];
								
								global $post;
								
								$categories = get_the_category();
						?>
							<!-- POST -->
							<div class="postPaginaInicial">
								<!-- IMAGEM DESTAQUE DO BANNER -->
								<a href="<?php echo get_the_permalink(); ?>">
									<figure style="background: url(<?php echo $fotoDestaquePost; ?>);">
										<img src="<?php echo $fotoDestaquePost; ?>" alt="Imagem">
									</figure>
								</a>	
								<!-- INFORMAÇÕES DO POST -->
								<article>
									<!-- CATEGORIA DO POST -->
									<?php 
										foreach ($categories as $categories){
											if ($categories->name != "destaque" && $categories->name != "Sem categoria" ){
												$nomeCategoria = $categories->name;
											}
										} 
									?>
									<a href="<?php echo get_the_permalink(); ?>"><h3><?php echo $nomeCategoria; ?></h3></a>
									<!-- TÍTULO DO POST-->
									<a href="<?php echo get_the_permalink(); ?>"><h2><?php echo get_the_title(); ?></h2></a>
									<!-- DATA DO POST -->
									<span class="dataDoPost"><?php echo  get_the_date('j F, Y'); ?></span>
									<!-- DESCRIÇÃO DO POST -->
									<p><?php customExcerpt(200); ?></p>
								</article>
							</div>
						<?php endwhile;endif; wp_reset_query(); ?>
						
					</section>
					<!-- DIV PAGINADOR -->
					<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
				</div>
				<div class="col-md-3">
					<?php get_sidebar(); ?>
					
				</div>
			</div>

			<!-- BANNER PROPAGANDA -->
			<div class="sessaoBannerPropaganda">
				<!-- LINK DO BANNER -->
				<a href="<?php echo $configuracao['opt_linkPropaganda'] ?>">
					<!-- IMAGEM DO BANNER -->
					<figure style="background: url(<?php echo $configuracao['opt_bannerPropaganda']['url'] ?>);">
						<img src="<?php echo $configuracao['opt_bannerPropaganda']['url'] ?>" alt="Banner Propaganda">
					</figure>
				</a>
			</div>

			
			<a href="#" id="abrirNewsletter">abrir</a>
		</div>
	</div>

<?php

get_footer();
