<?php

/**
 * Plugin Name: Base Cris e Cia
 * Description: Controle base do tema Nome do Projeto.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


	function baseProjeto () {

		// TIPOS DE CONTEÚDO
		conteudosProjeto();

		taxonomiaProjeto();

		metaboxesProjeto();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosProjeto (){

		// TIPOS DE CONTEÚDO
		//tipoDestaque();
		
		// TIPOS DE CONTEÚDO
		tipoProdutos();


		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;
				
				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoDestaque() {

		$rotulosDestaque = array(
								'name'               => 'Destaques',
								'singular_name'      => 'destaque',
								'menu_name'          => 'Destaques',
								'name_admin_bar'     => 'Destaques',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo destaque',
								'new_item'           => 'Novo destaque',
								'edit_item'          => 'Editar destaque',
								'view_item'          => 'Ver destaque',
								'all_items'          => 'Todos os destaques',
								'search_items'       => 'Buscar destaque',
								'parent_item_colon'  => 'Dos destaques',
								'not_found'          => 'Nenhum destaque cadastrado.',
								'not_found_in_trash' => 'Nenhum destaque na lixeira.'
							);

		$argsDestaque 	= array(
								'labels'             => $rotulosDestaque,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'destaque' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsDestaque);

	}

	// CUSTOM POST TYPE PRODUTOS
	function tipoProdutos() {

		$rotulosProdutos = array(
								'name'               => 'Produtos',
								'singular_name'      => 'produto',
								'menu_name'          => 'Produtos',
								'name_admin_bar'     => 'Produtos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo produto',
								'new_item'           => 'Novo produto',
								'edit_item'          => 'Editar produto',
								'view_item'          => 'Ver produto',
								'all_items'          => 'Todos os produtos',
								'search_items'       => 'Buscar produto',
								'parent_item_colon'  => 'Dos produtos',
								'not_found'          => 'Nenhum produto cadastrado.',
								'not_found_in_trash' => 'Nenhum produto na lixeira.'
							);

		$argsProdutos    = array(
								'labels'             => $rotulosProdutos,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-tag',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'produtos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('produtos', $argsProdutos);

	}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaProjeto () {		
		taxonomiaCategoriaDestaque();

		taxonomiaCategoriaProduto();
	}
		// TAXONOMIA DE DESTAQUE
		function taxonomiaCategoriaDestaque() {

			$rotulosCategoriaDestaque = array(
				'name'              => 'Categorias de destaque',
				'singular_name'     => 'Categoria de destaque',
				'search_items'      => 'Buscar categorias de destaque',
				'all_items'         => 'Todas as categorias de destaque',
				'parent_item'       => 'Categoria de destaque pai',
				'parent_item_colon' => 'Categoria de destaque pai:',
				'edit_item'         => 'Editar categoria de destaque',
				'update_item'       => 'Atualizar categoria de destaque',
				'add_new_item'      => 'Nova categoria de destaque',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de destaque',
				);

			$argsCategoriaDestaque 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaDestaque,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-destaque' ),
				);

			register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

		}	

		// TAXONOMIA DE PRODUTOS
		function taxonomiaCategoriaProduto() {

			$rotulosCategoriaProduto = array(
				'name'              => 'Categorias de produtos',
				'singular_name'     => 'Categoria de produto',
				'search_items'      => 'Buscar categorias de produtos',
				'all_items'         => 'Todas as categorias de produtos',
				'parent_item'       => 'Categoria de produto pai',
				'parent_item_colon' => 'Categoria de produto pai:',
				'edit_item'         => 'Editar categoria de produto',
				'update_item'       => 'Atualizar categoria de produto',
				'add_new_item'      => 'Nova categoria de produto',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de produtos',
			);

			$argsCategoriaProduto 	= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaProduto,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-produto' ),
			);

			register_taxonomy( 'categoriaProduto', array( 'produtos' ), $argsCategoriaProduto );

		}			

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesProjeto(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Crisecia_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaqueInicial',
				'title'			=> 'Detalhes do Destaque',
				'pages' 		=> array( 'destaque-inicial' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Foto Mobile: ',
						'id'    => "{$prefix}foto_mob",
						'desc'  => 'Foto mobile 320 X 480',
						'type'  => 'image_advanced',
						'max_file_uploads' => 1	
					),	
					
					array(
						'name'  => 'Vídeo: ',
						'id'    => "{$prefix}checkbox_destaqueinicial",
						'desc'  => 'Destaque do tipo vídeo marque aqui!',
						'type'  => 'checkbox'
					),
					array(
						'name'  => 'Imagem: ',
						'id'    => "{$prefix}checkbox_imagem",
						'desc'  => 'Marque está opção, se for arte pronta',
						'type'  => 'checkbox'
					),
					array(
						'name'  => 'Vídeo URL: ',
						'id'    => "{$prefix}video_destaqueinicial",
						'desc'  => 'Adicione o vídeo as mídias e adicione a url aqui!',
						'type'  => 'text'
					),	
									
				),
			);

			// METABOX DE PRODUTOS
			$metaboxes[] = array(

				'id'			=> 'metaboxProdutos',
				'title'			=> 'Detalhes do Produto',
				'pages' 		=> array( 'produtos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Preço do Produto',
						'id'    => "{$prefix}precoProduto",
						'desc'  => 'Insira aqui o preço do produto.',
						'type'  => 'text',
					),	
					array(
    					'type' => 'divider',
					),
					array(
						'name'  => 'Parcelamento',
						'id'    => "{$prefix}parcelamentoProduto",
						'desc'  => 'Insira aqui a quantidade de parcelas do produto.',
						'type'  => 'number',
					),
					array(
    					'type' => 'divider',
					),	
					array(
						'name'  => 'Desconto',
						'id'    => "{$prefix}descontoProduto",
						'desc'  => 'Insira aqui a porcentagem de desconto do produto.',
						'type'  => 'number',
					),	
					array(
    					'type' => 'divider',
					),
					array(
					    'name'  => 'Verificação',
					    'desc'  => 'Preencha este campo somente se houver desconto no produto.',
					    'id'    => "{$prefix}verificacaoCheckboxDesconto",
					    'type'  => 'checkbox',
					),
					array(
    					'type' => 'divider',
					),
					array(
					    'name'  => 'Classificação do produto',
					    'desc'  => 'Insira aqui a classificação do produto em uma escala de 1 a 5 estrelas.',
					    'id'    => "{$prefix}classificacaoProduto",
					    'type'  => 'number',
					),
					array(
    					'type' => 'divider',
					),
					array(
					    'name'  => 'Link do produto',
					    'desc'  => 'Insira aqui o link do produto.',
					    'id'    => "{$prefix}linkProduto",
					    'type'  => 'text',
					),
										
				),
			);

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesProjeto(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerProjeto(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseProjeto');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseProjeto();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );